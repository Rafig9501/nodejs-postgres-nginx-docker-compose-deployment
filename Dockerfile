FROM node:17-buster
RUN mkdir -p /usr/src/app
WORKDIR /usr/src/app
COPY package.json /usr/src/app/
RUN npm install
COPY . /usr/src/app
COPY check.sh /check.sh
EXPOSE 3000
RUN apt update -y && apt install netcat -y
CMD [ "/check.sh" ]